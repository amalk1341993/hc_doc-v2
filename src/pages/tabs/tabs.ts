import { Component } from '@angular/core';
import { IonicPage  } from 'ionic-angular';

@IonicPage()
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'WelcomePage';
  tab2Root = 'NotificationPage';
  tab3Root = 'AppointmentsPage';
  tab4Root = 'ProfilePage';

  constructor() {

  }
}
