import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConsultationdetailsPage } from './consultationdetails';

@NgModule({
  declarations: [
    ConsultationdetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ConsultationdetailsPage),
  ],
})
export class ConsultationdetailsPageModule {}
