import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaitentdetailPage } from './paitentdetail';

@NgModule({
  declarations: [
    PaitentdetailPage,
  ],
  imports: [
    IonicPageModule.forChild(PaitentdetailPage),
  ],
})
export class PaitentdetailPageModule {}
