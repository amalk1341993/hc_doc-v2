import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-medicinedetail',
  templateUrl: 'medicinedetail.html',
})
export class MedicinedetailPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MedicinedetailPage');
  }

  goBack(){
   this.navCtrl.pop();
 }

    mainmenu(){
    let modal = this.modalCtrl.create('MenuPage');
    modal.present();
  }

}
