import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MedicinedetailPage } from './medicinedetail';

@NgModule({
  declarations: [
    MedicinedetailPage,
  ],
  imports: [
    IonicPageModule.forChild(MedicinedetailPage),
  ],
})
export class MedicinedetailPageModule {}
