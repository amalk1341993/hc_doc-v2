import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConsultationhistoryPage } from './consultationhistory';

@NgModule({
  declarations: [
    ConsultationhistoryPage,
  ],
  imports: [
    IonicPageModule.forChild(ConsultationhistoryPage),
  ],
})
export class ConsultationhistoryPageModule {}
