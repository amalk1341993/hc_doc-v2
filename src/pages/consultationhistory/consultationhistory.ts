import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-consultationhistory',
  templateUrl: 'consultationhistory.html',
})
export class ConsultationhistoryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConsultationhistoryPage');
  }

  goBack(){
   this.navCtrl.pop();
 }

 open_page(page){
 this.navCtrl.push(page);
 }

}
